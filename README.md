# Proposals to Enhance Aptivate (PEAs)

Proposals to Enhance Aptivate (PEAs) are a formal way of proposing large changes to Aptivate.

We are currently trialing this as a process in the infrastructure working group.

Please see [PEA 1](https://git.coop/aptivate/governance/peas/merge_requests/1) for details.
