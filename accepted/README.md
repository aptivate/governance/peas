# Approved PEAs

PEAs that have been approved by Aptivate and are in the process of being
implemented. See [PEA 1](../draft/0001-pea-process.md) for details.
