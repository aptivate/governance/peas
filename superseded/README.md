# Superseded PEAs

PEAs that have been replaced/superseeded by newer PEAs. See [PEA
1](../draft/0001-pea-process.md) for details.
