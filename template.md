---

# How To Use This Template

This document provides a sample template for creating your own PEAs.

In conjunction with the content guidelines in [PEA 1](draft/0001-pea-process.md)
this should make it easy for you to conform your own PEAs to the format outlined below.

If you're unfamiliar with Markdown (the format required of PEAs), see this resource:

* [Learn X in Y Minutes: Markdown](https://learnxinyminutes.com/docs/markdown/)

***Once you've made a copy of this template, remove this section (between both
`---` markers), fill out the metadata below and the sections below, then submit
the PEA. Follow the guidelines in [PEA 1](draft/0001-pea-process.md).***

---

# PEA XXXX: PEA template

## Metadata

| PEA Number | Author(s) | Implementation Team | Shepherd(s) | Status | Created    |
| ---        | ---       | ---                 | ---         | ---    | ---        |
| XXXX       | XXX       | XXX                 | XXX         | Draft  | YYYY-MM-DD |

## Abstract

This should be a short (~200 word) description of the issue being addressed.

This (and the above metadata) is the only section strictly required to submit a
draft PEA; the following sections can be barebones and fleshed out as you work
through the PEA process.

## Motivation

This section should explain *why* this PEA is needed. The motivation is
critical for PEAs that want to add substantial new changes. It should clearly
explain why the existing solutions (if any) are inadequate to address the
problem that the PEA solves. PEA submissions without sufficient motivation may
be rejected outright.

## Specification

This section should contain a complete, detailed specification of the change.
The specification should be detailed enough to allow implementation -- that is,
Aptivate members other than the author should (given the right experience) be
able to independently implement the change, given only the PEA.

## Rationale

This section should flesh out out the specification by describing what
motivated the specific design and why particular design decisions were made. It
should describe alternate designs that were considered and related work.

The rationale should provide evidence of consensus within Aptivate and discuss
important objections or concerns raised during discussion.

## Backwards Compatibility

If this PEA introduces backwards incompatibilities (we use this term broadly to
mean organisational discontinuity of any kind), you must include this section.
It should describe these incompatibilities and their severity, and what
mitigation you plan to take to deal with these incompatibilities.

## Reference Implementation

If there's an implementation of the change under discussion in this PEA, this
section should include or link to that implementation and provide any notes
about installing/using/trying out the implementation.

## Copyright

This document has been placed in the public domain per the [Creative Commons
CC0 1.0 Universal license](http://creativecommons.org/publicdomain/zero/1.0/deed).
